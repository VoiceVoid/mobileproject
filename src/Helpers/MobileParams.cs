﻿namespace MobileProject.Helpers
{
    public class MobileParams
    {
        private const int MaxPageSize = 50;
        public int PageNumber { get; set; } = 1;
        private int pageSize = 10;
        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = (value > MaxPageSize) ? MaxPageSize : value; }
        }
        public string FilterByType { get; set; } = "";
        public string FilterByName{ get; set; } = "";

        public string Sorting { get; set; } = "name";
    }
}
