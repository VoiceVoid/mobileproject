﻿namespace MobileProject.Helpers
{
    public class PagedResult<T>
    {
        public int Page { get; set; }
        public int MaxPages { get; set; }
        public IEnumerable<T> Items { get; set; } = Enumerable.Empty<T>();

        public PagedResult<T> GetPagedList<T>(IQueryable<T> query, int pageNumber, int pageSize)
        {
            var startIndex = (pageNumber - 1) * pageSize;
            var items = query.Skip(startIndex).Take(pageSize).ToList();
            var count = query.Count();
            var pageCount = (int)Math.Ceiling(count / (double)pageSize);
            return new PagedResult<T>
            {
                Page = pageNumber,
                MaxPages = pageCount,
                Items = items
            };
        }
    }
}
