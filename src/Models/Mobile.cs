﻿using System.ComponentModel.DataAnnotations;

namespace MobileProject.Models
{
    public class Mobile
    {
        [Key]
        [Required]
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Image { get; set; } = string.Empty;
        [Required]
        public string Type { get; set; } = "android";
        public string AndroidStore { get; set; } = string.Empty;
        public string IosStore { get; set; } = string.Empty;
        public int Ratings { get; set; }
        public double Score { get; set; }
        public double Size { get; set; }
    }
}
