﻿using Newtonsoft.Json;

namespace MobileProject.Dtos
{
    public class MobileReadDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Image { get; set; } = string.Empty;
        public string Type { get; set; } = string.Empty;

        public string AndroidStore { get; set; } = string.Empty;

        public AndroidDetailsDto? AndroidDetails { get; set; }

        public string IosStore { get; set; } = string.Empty;

        public IosDetailsDto? IosDetails { get; set; }
    }
}
