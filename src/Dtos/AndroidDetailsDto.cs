﻿namespace MobileProject.Dtos
{
    public class AndroidDetailsDto
    {
        public int Ratings { get; set; }
        public double Score { get; set; }
    }
}
