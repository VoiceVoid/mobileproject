﻿namespace MobileProject.Dtos
{
    public class MobileDetailsDto
    {
        public int Ratings { get; set; }
        public double Score { get; set; }
        public double Size { get; set; }
    }
}
