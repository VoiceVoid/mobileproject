﻿namespace MobileProject.Dtos
{
    public class IosDetailsDto
    {
        public double Score { get; set; }
        public double Size { get; set; }
    }
}
