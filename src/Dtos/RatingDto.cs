﻿using System.ComponentModel.DataAnnotations;

namespace MobileProject.Dtos
{
    public class RatingDto
    {
        public int Id { get; set; }
        [Range(1, 5)]
        public int Score { get; set; }
    }
}
