﻿using System.ComponentModel.DataAnnotations;

namespace MobileProject.Dtos
{
    public class MobileCreateDto
    {
        public string Name { get; set; } = string.Empty;
        public string Image { get; set; } = string.Empty;
        [Required]
        public string Type { get; set; } = "android";
        public string AndroidStore { get; set; } = string.Empty;
        public string IosStore { get; set; } = string.Empty;
        public int Ratings { get; set; }
        [Range(1, 5)]
        public double Score { get; set; }
        public double Size { get; set; }
    }
}
