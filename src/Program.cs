using Microsoft.EntityFrameworkCore;
using MobileProject.Controllers;
using MobileProject.Data;
using MobileProject.Helpers;
using MobileProject.Modules;
using MobileProject.Repositories;
using MobileProject.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Runtime.Serialization.Formatters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<AppDbContext>(opt => opt.UseSqlServer(builder.Configuration["ConnectionStrings:MobileDb"]));
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMobile();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddControllers().AddNewtonsoftJson(options =>
{
    options.SerializerSettings.ContractResolver = new UnderscoreNamesContractResolver();
});

builder.Services.AddLogging(loggingBuilder =>
{
    loggingBuilder.ClearProviders(); // remove any existing providers
    loggingBuilder.AddFile("logs/myapp-{Date}.txt"); 
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

PrepDb.PrepPopulation(app);

app.Run();
