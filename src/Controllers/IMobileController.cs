﻿using Microsoft.AspNetCore.Mvc;
using MobileProject.Dtos;
using MobileProject.Helpers;

namespace MobileProject.Controllers
{
    public interface IMobileController
    {
        Task<ActionResult<PagedResult<MobileReadDto>>> GetAll(MobileParams mobileParams);
        Task<ActionResult<MobileReadDto>> GetById(int id);
        Task<ActionResult<MobileReadDto>> Create(MobileCreateDto mobile);
    }
}
