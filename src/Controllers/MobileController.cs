﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MobileProject.Dtos;
using MobileProject.Helpers;
using MobileProject.Models;
using MobileProject.Services;
using System.Reflection;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace MobileProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MobileController : ControllerBase, IMobileController
    {
        private readonly IMobileService _mobileService;
        private readonly IMapper _mapper;
        private readonly ILogger<MobileController> _logger;

        public MobileController(IMobileService mobileService, IMapper mapper, ILogger<MobileController> logger)
        {
            _mobileService = mobileService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<MobileReadDto>>> GetAll(MobileParams mobileParams)
        {
            _logger.LogInformation(@$"================================================================================================");
            _logger.LogInformation(@$"MobileController GetAll Received");

            var mobileList = await _mobileService.GetAllAsync(mobileParams);
            var mobilePagedResult = new PagedResult<Mobile>().GetPagedList(mobileList, mobileParams.PageNumber, mobileParams.PageSize);

            var mappedPagedList = new PagedResult<MobileReadDto>
            {
                Page = mobilePagedResult.Page,
                MaxPages = mobilePagedResult.MaxPages,
                Items = _mapper.Map<IEnumerable<MobileReadDto>>(mobilePagedResult.Items).ToList()
            };

            _logger.LogInformation(@$"================================================================================================");
            _logger.LogInformation(@$"MobileController GetAll Finished");

            return Ok(mappedPagedList);
        }

        [HttpPost]
        public async Task<ActionResult<MobileReadDto>> Create([FromBody] MobileCreateDto mobile)
        {
            _logger.LogInformation(@$"================================================================================================");
            _logger.LogInformation(@$"MobileController Create Received");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (string.IsNullOrEmpty(mobile.Type) || (mobile.Type.ToLower() != "ios" && mobile.Type.ToLower() != "android"))
            {
                return BadRequest("Invalid type parameter. Type should be either ios or android");
            }

            var mobileReadDto = await _mobileService.CreateAsync(mobile);

            _logger.LogInformation(@$"================================================================================================");
            _logger.LogInformation(@$"MobileController Create Finished");

            return CreatedAtRoute(nameof(GetById), new { id = mobileReadDto.Id }, mobileReadDto);
        }

        [HttpGet("{id}", Name = "GetById")]
        public async Task<ActionResult<MobileReadDto>> GetById(int id)
        {
            _logger.LogInformation(@$"================================================================================================");
            _logger.LogInformation(@$"MobileController GetById: {id} Received");

            var mobile = await _mobileService.GetByIdAsync(id);

            _logger.LogInformation(@$"================================================================================================");
            _logger.LogInformation(@$"MobileController GetById Finished");
            return Ok(mobile);
        }

        [HttpPost("update-rating")]
        public async Task<ActionResult<bool>> UpdateRating([FromBody]RatingDto ratingDto)
        {
            _logger.LogInformation(@$"================================================================================================");
            _logger.LogInformation(@$"MobileController UpdateRating Received");

            return await _mobileService.UpdateRatingAsync(ratingDto);
        }
    }
}
