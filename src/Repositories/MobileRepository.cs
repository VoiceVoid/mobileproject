﻿using Microsoft.EntityFrameworkCore;
using MobileProject.Data;
using MobileProject.Helpers;
using MobileProject.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System;

namespace MobileProject.Repositories
{
    public class MobileRepository : IMobileRepository
    {
        private AppDbContext _context;
        public MobileRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<Mobile>> GetAllAsync(MobileParams mobileParams)
        {
            var mobiles = _context.Mobiles.Where(m => 
            m.Type.Contains(mobileParams.FilterByType) ||
            m.Name.Contains(mobileParams.FilterByName));

            if (!string.IsNullOrEmpty(mobileParams.Sorting))
            {
                switch (mobileParams.Sorting.ToLower())
                {
                    case "name":
                        mobiles = mobiles.OrderBy(m => m.Name);
                        break;
                    case "name desc":
                        mobiles = mobiles.OrderByDescending(m => m.Name);
                        break;
                    case "score":
                        mobiles = mobiles.OrderBy(m => m.Score);
                        break;
                    case "score desc":
                        mobiles = mobiles.OrderByDescending(m => m.Score);
                        break;
                    default:
                        mobiles = mobiles.OrderBy(m => m.Id);
                        break;
                }
            }
            else
            {
                mobiles = mobiles.OrderBy(m => m.Id);
            }
            return mobiles;
           
        }

        public async Task<Mobile> GetByIdAsync(int id)
        {
            var mobile = await _context.Mobiles.FirstOrDefaultAsync(m => m.Id == id);
            if(mobile == null)
            {
                throw new ArgumentNullException(nameof(mobile));
            }
            return mobile;
        }

        public async Task CreateAsync(Mobile mobile)
        {
            if (mobile == null)
            {
                throw new ArgumentNullException(nameof(mobile));
            }
            await _context.Mobiles.AddAsync(mobile);
        }

        public void Update(Mobile mobile)
        {
            if (mobile == null)
            {
                throw new ArgumentNullException(nameof(mobile));
            }
             _context.Mobiles.Update(mobile);
        }


        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}
