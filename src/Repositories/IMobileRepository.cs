﻿using MobileProject.Helpers;
using MobileProject.Models;

namespace MobileProject.Repositories
{
    public interface IMobileRepository
    {
        Task<IQueryable<Mobile>> GetAllAsync(MobileParams mobileParams);
        Task<Mobile> GetByIdAsync(int id);
        Task CreateAsync(Mobile mobile);
        void Update(Mobile mobile);
        bool SaveChanges();
    }
}
