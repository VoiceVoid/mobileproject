﻿using AutoMapper;
using MobileProject.Dtos;
using MobileProject.Models;

namespace MobileProject.Profilers
{
    public class MobileProfiles : Profile
    {
        public MobileProfiles()
        {
            CreateMap<Mobile, MobileReadDto>()
                .ForMember(dest => dest.AndroidDetails, opt => opt.MapFrom(src =>
                    src.Type == "android" ? new AndroidDetailsDto
                    {
                        Ratings = src.Ratings,
                        Score = src.Score
                    } : null))
                .ForMember(dest => dest.IosDetails, opt => opt.MapFrom(src =>
                    src.Type == "ios" ? new IosDetailsDto
                    {
                        Score = src.Score,
                        Size = src.Size
                    } : null))
                .ForMember(dest => dest.AndroidStore, opt => opt.MapFrom(src =>
                    src.Type == "android" ? src.AndroidStore : null))
                .ForMember(dest => dest.IosStore, opt => opt.MapFrom(src =>
                    src.Type == "ios" ? src.IosStore : null));
            CreateMap<MobileCreateDto, Mobile>();

            CreateMap<AndroidDetailsDto, MobileDetailsDto>();
            CreateMap<IosDetailsDto, MobileDetailsDto>();
        }
    }
}
