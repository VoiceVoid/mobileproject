﻿using MobileProject.Dtos;
using MobileProject.Helpers;
using MobileProject.Models;

namespace MobileProject.Services
{
    public interface IMobileService
    {
        Task<MobileReadDto> CreateAsync(MobileCreateDto mobile);
        Task<IQueryable<Mobile>> GetAllAsync(MobileParams mobileParams);
        Task<MobileReadDto> GetByIdAsync(int id);
        bool Update(Mobile mobile);
        Task<bool> UpdateRatingAsync(RatingDto ratingDto);
    }
}
