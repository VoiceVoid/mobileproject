﻿using AutoMapper;
using MobileProject.Dtos;
using MobileProject.Helpers;
using MobileProject.Models;
using MobileProject.Repositories;

namespace MobileProject.Services
{
    public class MobileService : IMobileService
    {
        private readonly IMobileRepository _mobileRepo;
        private readonly IMapper _mapper;

        public MobileService(IMobileRepository mobileRepo, IMapper mapper)
        {
            _mobileRepo = mobileRepo;
            _mapper = mapper;
        }
        public async Task<IQueryable<Mobile>> GetAllAsync(MobileParams mobileParams)
        {

            return await _mobileRepo.GetAllAsync(mobileParams);
        }

        public async Task<MobileReadDto> GetByIdAsync(int id)
        {
            var mobile = await _mobileRepo.GetByIdAsync(id);
            return _mapper.Map<MobileReadDto>(mobile);
        }

        public async Task<MobileReadDto> CreateAsync(MobileCreateDto mobileCreate)
        {
            var mobile = _mapper.Map<Mobile>(mobileCreate);

            await _mobileRepo.CreateAsync(mobile);
            _mobileRepo.SaveChanges();

            var mobileReadDto = _mapper.Map<MobileReadDto>(mobile);

            
            return mobileReadDto;
        }

        public bool Update(Mobile mobile)
        {
            _mobileRepo.Update(mobile);
            return _mobileRepo.SaveChanges();
        }

        public async Task<bool> UpdateRatingAsync(RatingDto ratingDto)
        {
            var mobileToRate = await _mobileRepo.GetByIdAsync(ratingDto.Id);

            if(mobileToRate != null)
            {
                mobileToRate = AddRating(mobileToRate, ratingDto.Score);

                _mobileRepo.Update(mobileToRate);
                return _mobileRepo.SaveChanges();
            }
            return false;
        }

        private Mobile AddRating(Mobile mobile, double newRating)
        {
            double total = mobile.Score * mobile.Ratings;
            total += newRating;

            mobile.Ratings++;

            double newAverage = total / mobile.Ratings;

            mobile.Score = newAverage;
            return mobile;
        }
    }
}
