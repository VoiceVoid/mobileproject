﻿using MobileProject.Controllers;
using MobileProject.Repositories;
using MobileProject.Services;

namespace MobileProject.Modules
{
    public static class MobileExtensions
    {
        public static IServiceCollection AddMobile(this IServiceCollection services)
        {
            services.AddScoped<IMobileRepository, MobileRepository>();
            services.AddScoped<IMobileService, MobileService>();
            services.AddScoped<IMobileController, MobileController>();
            return services;
        }
    }
}
