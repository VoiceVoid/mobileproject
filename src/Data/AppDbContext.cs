﻿using Microsoft.EntityFrameworkCore;
using MobileProject.Models;

namespace MobileProject.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt)
        {
            this.Database.Migrate();
        }

        public DbSet<Mobile> Mobiles { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Mobile>().ToTable("Mobiles");
        }
    }
}
