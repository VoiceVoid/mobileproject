﻿using MobileProject.Models;

namespace MobileProject.Data
{
    public static class PrepDb
    {
        public static void PrepPopulation(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedData(serviceScope.ServiceProvider.GetService<AppDbContext>());
            }
        }

        private static void SeedData(AppDbContext? context)
        {
            if (context != null)
            {
                if (!context.Mobiles.Any())
                {
                    context.Mobiles.AddRange(
                        new Mobile { Name = "Cankarjev dom", Type = "ios", Image = "https://tehnik.telekom.si/PublishingImages/slovenske-aplikacije/Cankarjev-dom.png", AndroidStore = "", IosStore = "https://itunes.apple.com/si/app/daljinec+/id639982670?mt=8", Ratings = 3, Score = 2, Size = 1 },
                        new Mobile { Name = "Daljinec+", Type = "android", Image = "https://tehnik.telekom.si/PublishingImages/slovenske-aplikacije/DaljinecPlus.png", AndroidStore = "https://play.google.com/store/apps/details?id=cd.cc&hl=sl", IosStore = "", Ratings = 3, Score = 2, Size = 1 }
                        );
                    context.SaveChanges();
                }
            }
        }
    }
}
