using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MobileProject.Controllers;
using MobileProject.Dtos;
using MobileProject.Helpers;
using MobileProject.Models;
using MobileProject.Services;
using Moq;

namespace MobileProject.Tests.Controller
{
    public class MobileControllerTests
    {
        private readonly Mock<IMobileService> _mockMobileService;
        private readonly Mock<IMapper> _mockMapper;
        private readonly Mock<ILogger<MobileController>> _mockLogger;
        private readonly MobileController _controller;

        public MobileControllerTests()
        {
            _mockMobileService = new Mock<IMobileService>();
            _mockMapper = new Mock<IMapper>();
            _mockLogger = new Mock<ILogger<MobileController>>();
            _controller = new MobileController(_mockMobileService.Object, _mockMapper.Object, _mockLogger.Object);
        }

        [Fact]
        public async Task GetAll_Returns_PagedResultOfMobileReadDto()
        {
            // Arrange
            var mobileParams = new MobileParams { PageNumber = 1, PageSize = 10 };
            var mobileList = new List<Mobile>
        {
            new Mobile { Id = 1, Name = "Mobile 1", Type = "ios", Image = "string",AndroidStore = "strign", IosStore = "string",Ratings = 3, Score = 2, Size = 1},
            new Mobile { Id = 2, Name = "Mobile 2", Type = "android", Image = "string",AndroidStore = "strign", IosStore = "string",Ratings = 3, Score = 2, Size = 1}
        };
            var pagedMobileList = new PagedResult<Mobile>().GetPagedList(mobileList.AsQueryable(), mobileParams.PageNumber, mobileParams.PageSize);
            var mobileReadDtoList = new List<MobileReadDto>
        {
            new MobileReadDto { Id = 1, Name = "Mobile 1", Type = "ios", Image = "string", IosStore = "string", IosDetails = new IosDetailsDto{ Size = 1, Score = 2 } },
            new MobileReadDto { Id = 2, Name = "Mobile 2",  Type = "android", Image = "string",AndroidStore = "strign", AndroidDetails= new AndroidDetailsDto {Ratings = 3, Score = 2 }}
        };
            var pagedMobileReadDtoList = new PagedResult<MobileReadDto>
            {
                Page = pagedMobileList.Page,
                MaxPages = pagedMobileList.MaxPages,
                Items = mobileReadDtoList
            };
            _mockMobileService.Setup(s => s.GetAllAsync(mobileParams)).ReturnsAsync(mobileList.AsQueryable());
            _mockMapper.Setup(m => m.Map<IEnumerable<MobileReadDto>>(pagedMobileList.Items)).Returns(mobileReadDtoList);

            // Act
            var result = await _controller.GetAll(mobileParams);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var pagedResult = Assert.IsType<PagedResult<MobileReadDto>>(okResult.Value);
            Assert.Equal(pagedMobileReadDtoList.Page, pagedResult.Page);
            Assert.Equal(pagedMobileReadDtoList.MaxPages, pagedResult.MaxPages);
            Assert.Equal(pagedMobileReadDtoList.Items.Count(), pagedResult.Items.Count());
            Assert.Equal(pagedMobileReadDtoList.Items.First().Id, pagedResult.Items.First().Id);
            Assert.Equal(pagedMobileReadDtoList.Items.First().Name, pagedResult.Items.First().Name);
            Assert.Equal(pagedMobileReadDtoList.Items.Last().Id, pagedResult.Items.Last().Id);
            Assert.Equal(pagedMobileReadDtoList.Items.Last().Name, pagedResult.Items.Last().Name);
        }

        [Fact]
        public async Task Create_ValidMobile_ReturnsCreatedAtRouteResult()
        {
            // Arrange
            var mobileCreateDto = new MobileCreateDto
            {
                Name = "Cankarjev dom",
                Type = "ios",
                Image = "https://tehnik.telekom.si/PublishingImages/slovenske-aplikacije/Cankarjev-dom.png",
                AndroidStore = "",
                IosStore = "https://itunes.apple.com/si/app/daljinec+/id639982670?mt=8",
                Ratings = 3,
                Score = 2,
                Size = 1
            };

            var mobileReadDto = new MobileReadDto
            {
                Id = 1,
                Name = mobileCreateDto.Name,
                Type = mobileCreateDto.Type,
                Image = mobileCreateDto.Image,
                AndroidStore = mobileCreateDto.AndroidStore,
                IosStore = mobileCreateDto.IosStore,
                IosDetails = new IosDetailsDto { Size = mobileCreateDto.Size, Score = mobileCreateDto.Score }
            };

            _mockMobileService.Setup(svc => svc.CreateAsync(mobileCreateDto)).ReturnsAsync(mobileReadDto);

            // Act
            var result = await _controller.Create(mobileCreateDto);

            // Assert
            var createdAtRouteResult = Assert.IsType<CreatedAtRouteResult>(result.Result);
            var resultMobileReadDto = Assert.IsType<MobileReadDto>(createdAtRouteResult.Value);
            Assert.Equal(mobileReadDto, resultMobileReadDto);
        }

        [Fact]
        public async Task Create_InvalidMobile_ReturnsBadRequestResult()
        {
            // Arrange
            var mobileCreateDto = new MobileCreateDto
            {
                Name = "Cankarjev dom",
                Type = "",
                Image = "https://tehnik.telekom.si/PublishingImages/slovenske-aplikacije/Cankarjev-dom.png",
                AndroidStore = "",
                IosStore = "",
                Ratings = -1,
                Score = 7,
                Size = 1
            };

            _controller.ModelState.AddModelError("Type", "The Type field is required.");

            // Act
            var result = await _controller.Create(mobileCreateDto);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result.Result);
            Assert.IsType<SerializableError>(badRequestResult.Value);
        }
    }
}