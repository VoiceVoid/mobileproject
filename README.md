# MobileProject

This project uses .net 6 and sql express. To start the project you need to change connection string to your database. You also need to create MobileDb database in your sql server.

API CALL EXAMPLES:

**GET ALL:**
- sorting parameter accepts "name", "name desc", "score" or "score desc"

[GET]
https://localhost:7113/api/Mobile/

{
  "page_number": 1,
  "page_size": 10,
  "filter_by_type": "",
  "filter_by_name": "",
  "sorting": "score desc"
}

**GET BY ID:**

[GET]
https://localhost:7113/api/Mobile/1

**CREATE:**
- It also saves "type". By default type will be "android". It only accepts "ios" or "android".

[POST]
https://localhost:7113/api/Mobile/

{
  "name": "Cankarjev dom",
 // optional "type": "ios"
  "image": "https://tehnik.telekom.si/PublishingImages/slovenske-aplikacije/Cankarjev-dom.png",
  "android_store": "https://play.google.com/store/apps/details?id=cd.cc&hl=sl",
  "ios_store": "https://play.google.com/store/apps/details?id=cd.cc&hl=sl",
  "ratings":17,
  "score":4.5,
  "size":46.6
}

**RATING MOBILE:**

[POST]
https://localhost:7113/api/Mobile/update-rating

{
  "id": 1,
  "score": 5
}
